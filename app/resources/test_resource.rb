class TestResource < JSONAPI::Resource
  attributes :name, :status
  
  has_one :feature
end
