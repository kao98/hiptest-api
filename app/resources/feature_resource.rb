class FeatureResource < JSONAPI::Resource
  attributes :name, :description
  
  has_many :tests
end
