class Feature < ActiveRecord::Base
  has_many :tests, dependent: :destroy
end
